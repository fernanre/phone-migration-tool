from flask import current_app
from app.extensions import db
from app.models.users import User
from tests import BaseTestCase
from app.common.logger import setup_webapp_logs


class UserTest(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)

    def test_create_user(self):
        user = User(username="username_1",
                    person_id=12345,
                    email="username@cern.ch",
                    last_name="Perez",
                    first_name="Pepe",
                    is_admin=False)
        db.session.add(user)
        db.session.commit()

        users = User.query.all()

        self.assertEqual(len(users), 1)
        current_app.logger.debug(users[0].to_json())
        self.assertEqual(str(users[0]), "<User(1, username@cern.ch, person_id=12345): Pepe Perez>")
        self.assertEqual(users[0].to_json(), {'last_name': 'Perez', 'id': 1, 'first_name': 'Pepe', 'is_admin': False,
                                              'email': 'username@cern.ch'})
