import os
from flask_testing import TestCase
from flask import request, current_app
from app.app_factory import create_app
from app.common.logger import setup_webapp_logs
from app.extensions import db

_basedir = os.path.abspath(os.path.dirname(__file__))


class TestConfig(object):
    TESTING = True
    WTF_CSRF_ENABLED = False
    WTF_CSRF_SECRET_KEY = "somethingimpossibletoguess"
    SECRET_KEY = 'This string will be replaced with a proper key in production.'
    APPLICATION_ROOT = "/"
    CERN_OAUTH_CLIENT_ID = "test"
    CERN_OAUTH_CLIENT_SECRET = "test"
    CERN_OAUTH_TOKEN_URL = ""
    CERN_OAUTH_AUTHORIZE_URL = ""

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'test_app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = False

    APP_PORT = 8080
    DEBUG = True
    # Available values:
    # DEV: Development purposes with DEBUG level
    # PROD: Production purposes with INFO level
    LOG_LEVEL = "DEV"

    # Needed to use Oauth
    USE_PROXY = True

    ANALYTICS_GOOGLE_ID = ""
    ANALYTICS_PIWIK_ID = ""

    #
    # CACHE
    #
    CACHE_ENABLE = False
    CACHE_REDIS_PASSWORD = ""
    CACHE_REDIS_HOST = "redis"
    CACHE_REDIS_PORT = "6379"
    CACHE_OAUTH_TIMEOUT = 300

    if os.environ.get('CI_PROJECT_DIR', None):
        TEMP_FILES_DEST = os.path.join(os.environ.get('CI_PROJECT_DIR'), 'web', 'tests', 'samples')
    else:
        TEMP_FILES_DEST = os.path.join("/", "opt", "app-root", 'tests', 'samples')


class BaseTestCase(TestCase):
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'test_app.db')

    def create_app(self):

        application = create_app(TestConfig)
        setup_webapp_logs(application, to_file=False)

        return application

    def setUp(self):
        # with self.app.app_context():
        current_app.logger.debug("Creating database")
        db.create_all()

    def tearDown(self):
        # with self.app.app_context():
        current_app.logger.debug("Removing database")
        db.session.remove()
        db.drop_all()
