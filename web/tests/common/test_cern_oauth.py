import os
from flask import Flask
from flask_testing import TestCase

from flask import current_app

from app.common.cern_oauth import load_cern_oauth
from app.common.logger import setup_webapp_logs
from tests import BaseTestCase

_basedir = os.path.abspath(os.path.dirname(__file__))


class CernOauthTest(TestCase):
    TESTING = True
    WTF_CSRF_ENABLED = False
    WTF_CSRF_SECRET_KEY = "somethingimpossibletoguess"
    SECRET_KEY = 'This string will be replaced with a proper key in production.'
    APPLICATION_ROOT = "/"
    CERN_OAUTH_CLIENT_ID = "test"
    CERN_OAUTH_CLIENT_SECRET = "test"
    CERN_OAUTH_TOKEN_URL = ""
    CERN_OAUTH_AUTHORIZE_URL = ""

    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'test_app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = False

    APP_PORT = 8080
    DEBUG = True
    # Available values:
    # DEV: Development purposes with DEBUG level
    # PROD: Production purposes with INFO level
    LOG_LEVEL = "DEV"

    # Needed to use Oauth
    USE_PROXY = True

    ANALYTICS_GOOGLE_ID = ""
    ANALYTICS_PIWIK_ID = ""

    #
    # CACHE
    #
    CACHE_ENABLE = False
    CACHE_REDIS_PASSWORD = ""
    CACHE_REDIS_HOST = "redis"
    CACHE_REDIS_PORT = "6379"
    CACHE_OAUTH_TIMEOUT = 300

    if os.environ.get('CI_PROJECT_DIR', None):
        TEMP_FILES_DEST = os.path.join(os.environ.get('CI_PROJECT_DIR'), 'web', 'tests', 'samples')
    else:
        TEMP_FILES_DEST = os.path.join("/", "opt", "app-root", 'tests', 'samples')

    def create_app(self):

        application = Flask(__name__)
        application.config.from_object(self)

        setup_webapp_logs(application, to_file=False)

        return application

    def test_load_oauth(self):
        load_cern_oauth(current_app)
        pass