from flask import current_app
from tests import BaseTestCase
from app.common.logger import setup_webapp_logs


class LoggerTest(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)

    def test_setup_logger_stdout(self):
        setup_webapp_logs(current_app, to_file=False)
        pass

    def test_setup_logger_file(self):
        setup_webapp_logs(current_app, to_file=True, webapp_log_path='./test.log')
        pass

    def test_setup_logger_email(self):
        current_app.config["SEND_EMAIL"] = True
        setup_webapp_logs(current_app, to_file=True, webapp_log_path='./test.log')
        pass
