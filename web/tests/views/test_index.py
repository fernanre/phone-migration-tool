from flask import url_for, current_app
from tests import BaseTestCase


class UsersTest(BaseTestCase):
    def setUp(self):
        BaseTestCase.setUp(self)

    def test_index_reachable(self):
        response = self.client.get(url_for('frontend.main.index'))
        self.assertEqual(response.status_code, 200)

