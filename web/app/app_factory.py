from importlib import import_module

from flask import session, request, render_template

from app.admin.views import UserModelView
from app.common.cern_oauth import load_cern_oauth
from werkzeug.contrib.fixers import ProxyFix
from flask import Flask
from flask_migrate import Migrate
from flask_admin.contrib.sqla import ModelView

from app.extensions import db, assets, cache, admin
from app.static_assets import register_assets
from app.views.blueprints import all_blueprints
from app.secret import config
from app.models.users import User


def create_app(config_filename):
    """
    Factory to create the application using a file
    
    :param config_filename: The name of the file that will be used for configuration.
    :return: The created application
    """
    app = Flask(__name__)
    app.config.from_object(config_filename)

    if app.config.get('USE_PROXY', False):
        app.wsgi_app = ProxyFix(app.wsgi_app)

    """
    Load all the available models
    """
    with app.app_context():
        for app_module in app.config.get('DB_MODELS_IMPORTS', list()):
            import_module(app_module)

    """
    Enable the CERN Oauth Authentication
    """
    app.oauth = load_cern_oauth(app)

    """
    Register all the static assets and compile them
    """
    assets.init_app(app)
    with app.app_context():
        register_assets(assets)

    """
    Initialize the database and the migrations
    """
    db.init_app(app)
    Migrate(app, db)
    db.app = app

    _initialize_views(app)

    """
    Initialize Cache
    """
    cache.init_app(app)

    """
    Initialize Admin
    """
    admin.init_app(app)
    admin.add_view(UserModelView(User, db.session))

    @app.context_processor
    def inject_conf_var():
        """
        Injects all the application's global configuration variables
        
        :return: A dict with all the environment variables
        """

        return dict(
            #
            # Analytics
            #
            ANALYTICS_GOOGLE_ID=config.ANALYTICS_GOOGLE_ID,
            ANALYTICS_PIWIK_ID=config.ANALYTICS_PIWIK_ID,
            DEBUG=app.config['DEBUG']
            )

    @app.before_first_request
    def setup_database(*args, **kwargs):
        """
        Creates the database if it doesn't exist before the first request.
        
        :param args: 
        :param kwargs: 
        :return: 
        """
        with app.app_context():
            db.create_all()

    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('404.html'), 404

    app.logger.debug("Application finished loading.")

    return app


def _initialize_views(application):
    """
    Initializes all the application blueprints
    
    :param application: Application where the blueprints will be registered
    :return: 
    """

    for bp in all_blueprints:
        import_module(bp.import_name)
        application.register_blueprint(bp)
