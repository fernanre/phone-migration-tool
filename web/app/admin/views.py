from flask_admin.contrib import sqla
from flask_login import (
    LoginManager, current_user, login_user
)
from flask import current_app


# Create customized model view class
class UserModelView(sqla.ModelView):

    def is_accessible(self):
        if not current_user.is_active or not current_user.is_authenticated:
            return False

        if current_user.is_admin:
            current_app.logger.debug("It is")
            return True

        return False
