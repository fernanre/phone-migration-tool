from logging.handlers import RotatingFileHandler
from datetime import datetime
import logging
from pytz import timezone, utc
import sys


def setup_webapp_logs(app, to_file=True, webapp_log_path=""):
    """
    Configures the application logging
    
    :param webapp_log_path: 
    :param app: Flask application where the logging will be configured
    :param to_file: Whether to write the log to a file or not
    :return: 
    """
    with app.app_context():
        app.logger.handlers = []

        if app.config.get('LOG_LEVEL', 'DEV') == 'DEV':
            app.logger.setLevel(logging.DEBUG)

        if app.config.get('LOG_LEVEL', 'DEV') == 'PROD':
            app.logger.setLevel(logging.INFO)

        formatter = logging.Formatter(
            '%(levelname)s - %(asctime)s - %(name)s - %(message)s - %(pathname)s - %(funcName)s():%(lineno)d')

        def zurich_time(*args):
            utc_dt = utc.localize(datetime.utcnow())
            my_tz = timezone("Europe/Zurich")
            converted = utc_dt.astimezone(my_tz)
            return converted.timetuple()

        logging.Formatter.converter = zurich_time

        configure_stdout_logging(logger=app.logger, app=app, formatter=formatter)

        if to_file:
            print("Logging to file -> True")
            configure_file_logging(logger=app.logger, file_path=webapp_log_path, formatter=formatter, app=app)

        if app.config.get('SEND_EMAIL'):
            configure_email_logging(logger=app.logger, app=app)


def configure_file_logging(logger=None, file_path=None, formatter=None, app=None):
    error_on_eos = False
    handler = None
    try:
        handler = logging.handlers.TimedRotatingFileHandler(file_path, when='W6', backupCount=5)
        handler.setFormatter(formatter)
    except Exception as e:
        error_on_eos = True
        print("It seems there is a problem creating the log file. Logs will be only on stdout. Error: {}".format(e))

    if app.config.get('LOG_LEVEL', 'DEV') == 'DEV':
        if not error_on_eos:
            handler.setLevel(logging.DEBUG)

    if app.config.get('LOG_LEVEL', 'DEV') == 'PROD':
        if not error_on_eos:
            handler.setLevel(logging.INFO)

    if not error_on_eos:
        logger.addHandler(handler)


def configure_stdout_logging(logger=None, app=None, formatter=None):

    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if app.config.get('LOG_LEVEL', 'DEV') == 'DEV':
        stream_handler.setLevel(logging.DEBUG)
    if app.config.get('LOG_LEVEL', 'DEV') == 'PROD':
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)


def configure_email_logging(logger=None, app=None):
    fmt_email = logging.Formatter("""
            Message type:  %(levelname)s
            Name:          %(name)s
            Location:      %(pathname)s:%(lineno)d
            Module:        %(module)s/%(filename)s
            Function:      %(funcName)s
            Time:          %(asctime)s
            Message:

            %(message)s
        """)

    # email in case of errors
    mail_handler = logging.handlers.SMTPHandler(app.config.get('MAIL_HOSTNAME'),
                                                app.config.get('MAIL_FROM'),
                                                app.config.get('MAIL_TO'), "[WEBCAST WEBSITE] Web webapp error")
    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(fmt_email)
    logger.addHandler(mail_handler)
