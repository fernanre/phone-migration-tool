import os
import sqlalchemy
from flask import flash, current_app
from flask_dance import OAuth2ConsumerBlueprint
from flask_dance.consumer import oauth_authorized
from flask_dance.consumer.backend.sqla import SQLAlchemyBackend
from flask_login import (
    LoginManager, current_user, login_user
)
from sqlalchemy.orm.exc import NoResultFound

from app.extensions import db, cache
from app.models.users import User, OAuth
from app.secret import config


@cache.memoize(timeout=config.CACHE_OAUTH_TIMEOUT)
def get_user_egroups(user_id):
    """
    Retrieves the egroups of the user from the ouath api or from the cache
    
    :param user_id: Required by memoize to distinguish the egroups to be obtained. it has to be an integer
    :return: 
    """
    egroups_info = []
    if user_id:
        egroups_info = current_app.oauth.session.get('https://oauthresource.web.cern.ch/api/Groups')
    return egroups_info


def load_cern_oauth(app):
    """
    Loads the CERN Oauth into the application
    
    :param app: Flask application where the CERN Oauth will be loaded
    :return: 
    """
    oauth = OAuth2ConsumerBlueprint(
        'cern_oauth',
        __name__,
        url_prefix='/oauth',
        # oauth specific settings
        token_url=app.config.get('CERN_OAUTH_TOKEN_URL'),
        authorization_url=app.config.get('CERN_OAUTH_AUTHORIZE_URL'),
        # local urls
        login_url='/cern',
        authorized_url='/cern/authorized',
        client_id=app.config.get('CERN_OAUTH_CLIENT_ID', ''),
        client_secret=app.config.get('CERN_OAUTH_CLIENT_SECRET', '')
    )

    app.register_blueprint(oauth)

    oauth.backend = SQLAlchemyBackend(OAuth, db.session, user=current_user)

    # setup login manager
    login_manager = LoginManager()
    login_manager.login_view = 'cern_oauth.login'

    @login_manager.user_loader
    def load_user(user_id):
        try:
            return User.query.get(int(user_id))
        except sqlalchemy.exc.InternalError as e:
            current_app.logger.warning(str(e))
            return None

    login_manager.init_app(app)

    @oauth_authorized.connect_via(oauth)
    def cern_logged_in(bp, token):
        # We don't keep the OAuth token since it's excessively long (~3kb) and we don't need
        # it anymore after getting the data here.
        response = oauth.session.get('https://oauthresource.web.cern.ch/api/User')
        response.raise_for_status()
        data = response.json()
        is_admin = False

        # flash("You are {first_name}".format(first_name=data['first_name'].strip()))

        query = User.query.filter_by(username=data['username'].strip())

        try:
            existing_user = query.one()
            current_app.logger.info(
                "User {} was found with person_id {}".format(existing_user.username, existing_user.person_id))
        except NoResultFound:
            existing_user = User(username=data['username'].strip(),
                                 person_id=data['personid'],
                                 email=data['email'].strip(),
                                 last_name=data['last_name'].strip(),
                                 first_name=data['first_name'].strip(),
                                 is_admin=is_admin)
            db.session.add(existing_user)
            db.session.commit()
        if login_user(existing_user):
            current_app.logger.info(
                "Login user {} with person_id: {}".format(existing_user.username, existing_user.person_id))

        egroups_info = get_user_egroups(existing_user.id)

        admin_egroup_found = False
        for egroup_name in egroups_info.json()['groups']:
            # Check if the user is admin
            if egroup_name == current_app.config.get('ADMIN_EGROUP'):
                admin_egroup_found = True
                existing_user.is_admin = True
        if not admin_egroup_found:
            existing_user.is_admin = False

        db.session.commit()

        app.logger.info('OAuth login successful for %s (%s #%d)', data['username'], data['email'],
                        data['personid'])

    return oauth
