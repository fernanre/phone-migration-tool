import click

from app.extensions import db


def initialize_cli(app):
    """
    Add additional commands to the CLI. These are loaded automatically on the main.py

    :param app: App to attach the cli commands to
    :return: None
    """

    @app.cli.command()
    def init_db():
        """
        Initialize the database.
        """
        click.echo('Initializing the db')
        db.create_all()

    @app.cli.command()
    def clear_db():
        """
        Removes all the tables of the database and their content
        """
        click.echo('Clearing the db')
        db.drop_all()
        click.echo('Initializing the db')
        db.create_all()
