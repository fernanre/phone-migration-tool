#
# Application
#
DEBUG = False
IS_LOCAL_INSTALLATION = True
TESTING = False
USE_PROXY = True
APP_PORT = 8080
SECRET_KEY = "E\xf0\xd2G\xd5\x0bJ\xfd\x0b\xc7\xdc\x8c\xfb\xbd\xf7\xcd&C\xa3\xbc\xc8\xf7\xeb5"


#
# Authorization
#
ADMIN_EGROUP = "it-cda-ic"

#
# Analytics
#
ANALYTICS_GOOGLE_ID = ""
ANALYTICS_PIWIK_ID = ""

#
# LOGS
#
LOG_LEVEL = "DEV"
WEBAPP_LOGS = "/opt/app-root/logs/webapp.log"
SEND_EMAIL = True
MAIL_HOSTNAME = ""
MAIL_FROM = ""
MAIL_TO = ""

#
# OAUTH
#
CERN_OAUTH_CLIENT_ID = ""
CERN_OAUTH_CLIENT_SECRET = ""
CERN_OAUTH_AUTHORIZE_URL = "https://oauth.web.cern.ch/OAuth/Authorize"
CERN_OAUTH_TOKEN_URL = "https://oauth.web.cern.ch/OAuth/Token"

#
# Database
#
DB_NAME = "postgres"
DB_PASS = "postgres"
DB_PORT = 5432
DB_SERVICE = "postgres"
DB_USER = "postgres"

SQLALCHEMY_POOL_SIZE = 5
SQLALCHEMY_POOL_TIMEOUT = 10
SQLALCHEMY_POOL_RECYCLE = 120
SQLALCHEMY_MAX_OVERFLOW = 3

#
# CACHE
#
CACHE_ENABLE = False
CACHE_REDIS_PASSWORD = ""
CACHE_REDIS_HOST = "redis"
CACHE_REDIS_PORT = "6379"
CACHE_OAUTH_TIMEOUT = 300
