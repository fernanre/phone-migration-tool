from app.common.logger import setup_webapp_logs

from app.common.cli import initialize_cli
from app.settings import BaseConfig
from app.app_factory import create_app
from app.secret import config
import urllib3

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

"""
This file is the one loaded on development.
Check the README file to learn how to generate the certificates.
"""

app = create_app(BaseConfig)

setup_webapp_logs(app, webapp_log_path=config.WEBAPP_LOGS)
initialize_cli(app)

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=app.config['APP_PORT'], debug=app.config['DEBUG'],
            ssl_context=('/opt/app-root/ssl/cert.pem', '/opt/app-root/ssl/key.pem'))
