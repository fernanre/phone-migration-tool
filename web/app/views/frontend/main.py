from app.views.blueprints import frontend_blueprint
from flask import render_template, redirect, url_for
from flask_login import login_required, logout_user


@frontend_blueprint.route('/')
def index():
    """
    Homepage view
    :return: 
    """

    return render_template('frontend/index.html')


@frontend_blueprint.route('/logout')
@login_required
def logout():
    """
    View to log out a user on the site
    :return: Redirects to the users.index after logout
    """
    logout_user()
    return redirect(url_for('frontend.main.index'))
