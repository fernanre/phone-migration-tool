from flask import Blueprint


def _blueprint_factory(partial_module_string, url_prefix):
    """
    Generates blueprint objects for view modules.
    
    :param partial_module_string:  String representing a view module without the absolute path (e.g. 'home.index' for pypi_portal.views.home.index).
    :param url_prefix: URL prefix passed to the blueprint.
    :return: Blueprint instance for a view module.
    """
    name = partial_module_string
    import_name = 'app.views.{}'.format(partial_module_string)
    template_folder = 'templates'
    blueprint = Blueprint(name, import_name, url_prefix=url_prefix)
    return blueprint


# Put user's frontend blueprints here
frontend_blueprint = _blueprint_factory('frontend.main', '')

all_blueprints = (
    frontend_blueprint,
)
