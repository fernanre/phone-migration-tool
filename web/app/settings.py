from app.secret import config


class OauthConfig(object):
    """
    CERN Oauth configuration
    """
    CERN_OAUTH_CLIENT_ID = config.CERN_OAUTH_CLIENT_ID
    CERN_OAUTH_CLIENT_SECRET = config.CERN_OAUTH_CLIENT_SECRET
    CERN_OAUTH_AUTHORIZE_URL = config.CERN_OAUTH_AUTHORIZE_URL
    CERN_OAUTH_TOKEN_URL = config.CERN_OAUTH_TOKEN_URL


class DatabaseConfig(object):
    """
    Application database configuration
    """
    DB_NAME = config.DB_NAME
    DB_PASS = config.DB_PASS
    DB_PORT = config.DB_PORT
    DB_SERVICE = config.DB_SERVICE
    DB_USER = config.DB_USER

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = False
    SQLALCHEMY_POOL_SIZE = config.SQLALCHEMY_POOL_SIZE
    SQLALCHEMY_POOL_TIMEOUT = config.SQLALCHEMY_POOL_TIMEOUT
    SQLALCHEMY_POOL_RECYCLE = config.SQLALCHEMY_POOL_RECYCLE
    SQLALCHEMY_MAX_OVERFLOW = config.SQLALCHEMY_MAX_OVERFLOW

    """
    Database URI will be generated with the already gotten database parameters
    """
    SQLALCHEMY_DATABASE_URI = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
        DB_USER, DB_PASS, DB_SERVICE, DB_PORT, DB_NAME
    )


class BaseConfig(OauthConfig,
                 DatabaseConfig):
    """
    Stores the app configuration
    """

    ADMIN_EGROUP = config.ADMIN_EGROUP
    APP_PORT = config.APP_PORT
    SECRET_KEY = config.SECRET_KEY

    """
    Debug and logging configuration
    """
    DEBUG = config.DEBUG
    IS_LOCAL_INSTALLATION = config.IS_LOCAL_INSTALLATION
    TESTING = config.TESTING

    SEND_EMAIL = config.SEND_EMAIL
    MAIL_HOSTNAME = config.MAIL_HOSTNAME
    MAIL_FROM = config.MAIL_FROM
    MAIL_TO = config.MAIL_TO

    LOG_LEVEL = config.LOG_LEVEL
    WEBAPP_LOGS = config.WEBAPP_LOGS

    """
    Whether or not to use the wsgi Proxy Fix
    """
    USE_PROXY = config.USE_PROXY
    try:
        SERVER_NAME = config.SERVER_NAME
    except AttributeError:
        pass

    """
    Add here the model modules to be loaded automatically on the application factory
    """
    DB_MODELS_IMPORTS = (
        'app.models.users',
    )
