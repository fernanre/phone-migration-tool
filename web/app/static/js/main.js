/**
 * Main JS.
 */

$(document).ready(function () {
    $('.ui.dropdown').dropdown();

    // create sidebar and attach to menu open
    $('.ui.sidebar')
        .sidebar('attach events', '.toc.item')
    ;

});