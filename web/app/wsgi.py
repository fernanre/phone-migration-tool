from app.common.logger import setup_webapp_logs
from app.settings import BaseConfig
from app.app_factory import create_app
from app.common.cli import initialize_cli
from app.secret import config
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

"""
This file is the one loaded on Openshift using uWsgi.
"""

app = create_app(BaseConfig)
#
# Setting up logs
#
setup_webapp_logs(app, webapp_log_path=config.WEBAPP_LOGS)

initialize_cli(app)
