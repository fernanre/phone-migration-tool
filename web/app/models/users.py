from flask_login import UserMixin
from flask_dance.consumer.backend.sqla import OAuthConsumerMixin
from app.extensions import db


class User(db.Model, UserMixin):
    """
    Represents an application User
    """
    __tablename__ = 'user'
    __table_args__ = (db.CheckConstraint('email = lower(email)', 'lowercase_email'),)

    id = db.Column(db.Integer, primary_key=True)
    person_id = db.Column(db.Integer, nullable=False, index=True)
    first_name = db.Column(db.String(255), nullable=False)
    username = db.Column(db.String(255), nullable=False)
    last_name = db.Column(db.String(255), nullable=False)
    email = db.Column(db.String(255), nullable=False, unique=True)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)

    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())

    def __init__(self, username=None, person_id=None, email=None, last_name=None, first_name=None, is_admin=False):

        self.username = username
        self.person_id = person_id
        self.email = email
        self.last_name = last_name
        self.first_name = first_name
        self.is_admin = is_admin

    @property
    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def __repr__(self):
        admin = ', is_admin=True' if self.is_admin else ''
        return '<User({}, {}, person_id={}{}): {}>'.format(self.id, self.email, self.person_id, admin, self.name)

    def to_json(self):
        return {'id': self.id, 'first_name': self.first_name, 'last_name': self.last_name, 'email': self.email,
                'is_admin': self.is_admin}


class OAuth(db.Model, OAuthConsumerMixin):
    """
    Represents an Oauth connection in the application
    """
    __tablename__ = 'flask_dance_oauth'

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)
