import flask_assets
from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache
from app.secret import config
from flask_admin import Admin

#
# Database
#
db = SQLAlchemy()
#
# Static Assets
#
assets = flask_assets.Environment()

#
# Caching
#
cache = None
if config.CACHE_ENABLE:
    cache = Cache(config={'CACHE_TYPE': 'redis',
                          "CACHE_KEY_PREFIX": "webcast",
                          "CACHE_REDIS_HOST": config.CACHE_REDIS_HOST,
                          "CACHE_REDIS_PASSWORD": config.CACHE_REDIS_PASSWORD,
                          "CACHE_REDIS_PORT": config.CACHE_REDIS_PORT

                          })
else:
    cache = Cache(config={'CACHE_TYPE': 'null'})


#
# Admin
#
admin = Admin(name='Phone Migration Admin', template_mode='bootstrap3')
